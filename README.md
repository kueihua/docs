Note
-------------------------------------
1.  SW docs will be added as issues.
2.  Some pictures/texts are just grabbed from others, so the copyright is belong to original creator.

Content
-------------------------------------
1.  [How to branch google source code into gitlab?](https://gitlab.com/kueihua/docs/blob/master/system/How_to_branch_google_source_code_into_gitlab.md)
2.  [How to install android Pie to Redmi Note 3?](https://gitlab.com/kueihua/docs/blob/master/system/How_to_install_android_Pie_to_Redmi_Note_3.md)
3.  Audio docs
    * [Audio Framework](https://gitlab.com/kueihua/docs/blob/master/audio/Audio_Framework.md)
    * Audio Volume Adjustment
    * Audio Volume initial
    * Android launcher touch sound (key tone)
    * HP volume will be reset after AC off->on
    * Audio HAL device open flow
    * Modify audioflinger standby times
    * How to enable ASAN in audio HAL code?
    * [AudioFlinger heap size at low ram configuration (ro.config.low_ram=true)](https://gitlab.com/kueihua/docs/blob/master/audio/audioflinger_heap_size_at_low_ram.md)
4.  How to VTS and debug VTS?
    * How to CTS and debug CTS?
    * How to CTS Verifier and debug CTS verifier?
    * How to GTS and debug GTS?
    * How to de-compile APK?
5.  [Useful git command and linux commands](https://gitlab.com/kueihua/docs/blob/master/system/Useful_git_commands_and_linux_commands.md)
6.  How to be frfish from arrowOS?
7.  Android JAVA Info?
